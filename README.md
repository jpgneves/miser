# Miser

A parser, lexer and code generator for Apache Thrift IDL files, written in Haskell.

See [README.org](README.org)