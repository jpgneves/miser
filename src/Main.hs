module Main where

import System.Environment
import Text.Miser.Thrift.Parser
import Text.Miser.Haskell.Gen

import qualified Data.Map.Strict as M

main :: IO ()
main = do
  (file:_) <- getArgs
  result <- parseAll file
  case (snd (head $ M.toList result)) of
    Left e -> return ()
    Right d -> do
      writeFile "tree.txt" $ show result
      createHs d "foo.hs"
