module Language.Thrift.Types where

import Text.Miser.Thrift.Lexer

data Document = Document [Header] [Definition] deriving (Show)

data Definition =
  TypeDef DefinitionType Identifier
  | ConstDef FieldType Identifier ConstValue
  | StructDef Identifier [Field]
  | UnionDef Identifier [Field]
  | EnumDef Identifier [EnumElem]
  | ExceptionDef Identifier [Field]
  | ServiceDef Identifier (Maybe Identifier) [Function]
  deriving (Show)

data DefinitionType =
  BaseType String
  | MapType FieldType FieldType
  | ListType FieldType
  | SetType FieldType
  deriving (Show)

data ConstValue =
  IntConstant Integer
  | DoubleConstant Double
  | LiteralConstant Literal
  | IdentifierConstant Identifier
  | ListConstant [ConstValue]
  | MapConstant [(ConstValue, ConstValue)]
  deriving (Show)

data EnumElem = EnumElem Identifier (Maybe Integer) deriving (Show)

data Field = Field (Maybe FieldId) (Maybe FieldQualifier) FieldType Identifier (Maybe ConstValue) deriving (Show)

data FieldId = FieldId Integer deriving (Show)

data FieldQualifier =
  RequiredField
  | OptionalField
  deriving (Show)

data FieldType =
  IdentifierFieldType Identifier
  | DefinitionFieldType DefinitionType
  deriving (Show)

data Function = Function Bool FunctionType Identifier [Field] (Maybe Throws)
  deriving (Show)

data FunctionType =
  FunctionType FieldType
  | VoidFunctionType
  deriving (Show)

data Throws = Throws [Field]
  deriving (Show)

data Header =
  IncludeHeader Literal
  | CppIncludeHeader Literal
  | Namespace NamespaceScope Identifier
  deriving (Show)

data NamespaceScope = NamespaceScope String deriving (Show)
