{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE OverloadedStrings #-}

module Data.Thrift where

import Data.Int
import Data.Serialize
import Data.Typeable
import GHC.Generics
import GHC.TypeLits
import qualified Data.ByteString.Char8 as BS

newtype Field (i :: Nat) d = Field d
  deriving (Eq, Generic, Ord, Show)

instance (Serialize d) => Serialize (Field i d)

getField :: Field i a -> a
getField (Field a) = a

putField :: a -> Field i a
putField = Field

data MessageType = Call
                 | Reply
                 | Exception
                 | OneWay
  deriving (Generic, Show)

data Message (a :: MessageType) = Message { _name  :: BS.ByteString
                                          , _seqid :: Int32
                                          }
                                  deriving (Generic, Show)

data ApplicationException = ApplicationException { _message :: Field 1 BS.ByteString
                                                 , _type    :: Field 2 ExceptionType
                                                 }
                            deriving (Generic, Show)

data ExceptionType = UNKNOWN
                   | UNKNOWN_METHOD
                   | INVALID_MESSAGE_TYPE
                   | WRONG_METHOD_NAME
                   | BAD_SEQUENCE_ID
                   | MISSING_RESULT
                   | INTERNAL_ERROR
                   | PROTOCOL_ERROR
                   | INVALID_TRANSFORM
                   | INVALID_PROTOCOL
                   | UNSUPPORTED_CLIENT_TYPE
  deriving (Eq, Generic, Ord, Show, Typeable)

instance Enum ExceptionType where
  fromEnum UNKNOWN                 = 0
  fromEnum UNKNOWN_METHOD          = 1
  fromEnum INVALID_MESSAGE_TYPE    = 2
  fromEnum WRONG_METHOD_NAME       = 3
  fromEnum BAD_SEQUENCE_ID         = 4
  fromEnum MISSING_RESULT          = 5
  fromEnum INTERNAL_ERROR          = 6
  fromEnum PROTOCOL_ERROR          = 7
  fromEnum INVALID_TRANSFORM       = 8
  fromEnum INVALID_PROTOCOL        = 9
  fromEnum UNSUPPORTED_CLIENT_TYPE = 10
  toEnum 0  = UNKNOWN
  toEnum 1  = UNKNOWN_METHOD
  toEnum 2  = INVALID_MESSAGE_TYPE
  toEnum 3  = WRONG_METHOD_NAME
  toEnum 4  = BAD_SEQUENCE_ID
  toEnum 5  = MISSING_RESULT
  toEnum 6  = INTERNAL_ERROR
  toEnum 7  = PROTOCOL_ERROR
  toEnum 8  = INVALID_TRANSFORM
  toEnum 9  = INVALID_PROTOCOL
  toEnum 10 = UNSUPPORTED_CLIENT_TYPE
