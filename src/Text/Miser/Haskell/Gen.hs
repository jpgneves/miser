module Text.Miser.Haskell.Gen where

import Data.Char
import Data.List.Split
import Language.Haskell.Exts.Syntax
import Language.Haskell.Exts.Pretty
import Language.Thrift.Types
import Text.Miser.Thrift.Lexer hiding (Literal(..))-- TODO: Re-export this from Language.Thrift.Types
import qualified Text.Miser.Thrift.Lexer as T (Literal(..))
import System.IO

capitalize :: String -> String
capitalize (c:cs) = (toUpper c : cs)
capitalize "" = ""

decapitalize :: String -> String
decapitalize (c:cs) = (toLower c : cs)
decapitalize "" = ""

abbrev :: String -> String
abbrev str = filter isAsciiUpper str

baseTypeToHsType :: DefinitionType -> Type
baseTypeToHsType (BaseType "i8")     = TyCon (UnQual (Ident "Int8"))
baseTypeToHsType (BaseType "i16")    = TyCon (UnQual (Ident "Int16"))
baseTypeToHsType (BaseType "i32")    = TyCon (UnQual (Ident "Int32"))
baseTypeToHsType (BaseType "i64")    = TyCon (UnQual (Ident "Int64"))
baseTypeToHsType (BaseType "bool")   = TyCon (UnQual (Ident "Bool"))
baseTypeToHsType (BaseType "double") = TyCon (UnQual (Ident "Double"))
baseTypeToHsType (BaseType "string") = TyCon (Qual (ModuleName "BS") (Ident "ByteString"))

fieldTypeToType :: FieldType -> Type
fieldTypeToType (DefinitionFieldType t)              = thriftTypeToHaskellType t
fieldTypeToType (IdentifierFieldType (Identifier t)) = TyCon (UnQual (Ident $ capitalize t))

optionalFieldTypeToType :: FieldType -> Type
optionalFieldTypeToType t = TyParen (TyApp (TyCon (UnQual (Ident "Maybe")))
                                    (fieldTypeToType t))


thriftTypeToHaskellType :: DefinitionType -> Type
thriftTypeToHaskellType (ListType t)    = TyList $ fieldTypeToType t
thriftTypeToHaskellType (MapType kt vt) = TyParen (TyApp
                                                   (TyApp
                                                    (TyCon (Qual (ModuleName "M") (Ident "Map")))
                                                     (fieldTypeToType kt)
                                                   )
                                                   (fieldTypeToType vt)
                                                  )
thriftTypeToHaskellType (SetType t)     = TyApp
                                          (TyCon (Qual (ModuleName "S") (Ident "Set")))
                                          (fieldTypeToType t)
thriftTypeToHaskellType t               = baseTypeToHsType t

genTypeDef :: SrcLoc -> Definition -> [Decl]
genTypeDef srcloc (TypeDef t (Identifier name)) =
  [DataDecl srcloc NewType [] ident [] qualcondecl derivs]
  where
    ident = Ident $ capitalize name
    qualcondecl = [QualConDecl srcloc [] [] (ConDecl ident [thriftTypeToHaskellType t])]
    derivs = [ (UnQual (Ident "Show"), []) ]

constValueToHaskell :: FieldType -> ConstValue -> Exp
constValueToHaskell _ (IntConstant i)                 = Lit $ Int i
constValueToHaskell _ (DoubleConstant d)              = Lit $ Frac (toRational d)
constValueToHaskell _ (LiteralConstant (T.Literal s)) = Lit $ String s
constValueToHaskell _ (IdentifierConstant i) =
  Var (UnQual (Ident $ resolveIdentifierConst i))
constValueToHaskell (DefinitionFieldType (SetType t)) (ListConstant ls) =
  App (Var (Qual (ModuleName "S") (Ident "fromList")))
  (List $ map (constValueToHaskell t) ls)
constValueToHaskell t (ListConstant ls) =
  List $ map (constValueToHaskell t) ls
constValueToHaskell (DefinitionFieldType (MapType kt vt)) (MapConstant m) =
  App (Var (Qual (ModuleName "M") (Ident "fromList")))
  (List $ map (\(k, v) ->
                 Tuple Boxed [constValueToHaskell kt k, constValueToHaskell vt v]
              ) m)
constValueToHaskell t@(IdentifierFieldType (Identifier i)) (MapConstant m) =
  RecConstr (UnQual (Ident $ capitalize i))
  (map (\((LiteralConstant (T.Literal k)), v) ->
          FieldUpdate (UnQual (Ident $ genStructFieldName i k
                              )) (App (Var (UnQual $ Ident "putField")) (constValueToHaskell t v))) m)

genConstDef :: SrcLoc -> Definition -> [Decl]
genConstDef srcLoc (ConstDef t (Identifier name) value) =
  [
    TypeSig srcLoc [Ident canonicalName] (fieldTypeToType t)
  , PatBind srcLoc (PVar (Ident canonicalName)) (UnGuardedRhs (constValueToHaskell t value)) Nothing
  ]
  where canonicalName = genConstName name

resolveIdentifierConst :: Identifier -> String
resolveIdentifierConst (Identifier i) =
  case splitOn "." i of
    [namespace, identifier] -> concat [abbrev namespace, "_", identifier]
    [identifier]            -> genConstName identifier

genConstName :: String -> String
genConstName name =
  concat (header : body)
  where words = splitOn "_" name
        header = map toLower $ head words
        body = map (capitalize . (map toLower)) $ tail words

genStructFields :: String -> [Field] -> [([Name], Type)]
genStructFields structName fields =
  map (genStructField structName) fields

genStructField :: String -> Field -> ([Name], Type)
genStructField structName (Field (Just (FieldId i)) q t (Identifier n) _) =
  ([Ident canonicalName], (TyApp (TyApp (TyCon (UnQual $ Ident "Field")) (TyPromoted $ PromotedInteger i)) (fieldTypeFun t)))
  where
    canonicalName = genStructFieldName structName n
    fieldTypeFun = case q of
                     Just OptionalField -> optionalFieldTypeToType
                     otherwise          -> fieldTypeToType

genStructFieldName :: String -> String -> String
genStructFieldName structName fieldName = "_" ++ abbrev structName ++ "_" ++ fieldName

genStructDef :: SrcLoc -> Definition -> [Decl]
genStructDef srcloc (StructDef (Identifier name) fields) =
  [
    DataDecl srcloc DataType [] (Ident canonicalName) [] [recordDecl] derivs
  , InstDecl srcloc Nothing [] [] (Qual (ModuleName "Serialize") (Ident "Serialize")) [(TyCon (UnQual $ (Ident canonicalName)))] []
  ]
  where
    canonicalName = capitalize name
    recordDecl = QualConDecl srcloc [] [] (RecDecl (Ident canonicalName) (genStructFields canonicalName fields))
    derivs = [ (UnQual (Ident "Eq"), [])
             , (UnQual (Ident "Generic"), [])
             , (UnQual (Ident "Ord"), [])
             , (UnQual (Ident "Show"), [])
             ]

genUnionDef :: SrcLoc -> Definition -> [Decl]
genUnionDef srcLoc (UnionDef (Identifier name) fields) =
  [
    DataDecl srcLoc DataType [] canonicalName [] decls derivs
  , InstDecl srcLoc Nothing [] [] (Qual (ModuleName "Serialize") (Ident "Serialize")) [(TyCon (UnQual $ canonicalName))] []
  ]
  where
    prefix = abbrev $ capitalize name
    canonicalName = Ident $ capitalize name
    decls = map (\(Field _ _ t (Identifier n) _) -> QualConDecl srcLoc [] [] (ConDecl (Ident $ prefix ++ "_" ++ capitalize n) [fieldTypeToType t])) fields
    derivs = [ (UnQual (Ident "Eq"), [])
             , (UnQual (Ident "Generic"), [])
             , (UnQual (Ident "Ord"), [])
             , (UnQual $ Ident "Show", [])
             ]

genEnumDef :: SrcLoc -> Definition -> [Decl]
genEnumDef srcLoc (EnumDef (Identifier name) elems) =
  [
    DataDecl srcLoc DataType [] canonicalName [] decls derivs
  , InstDecl srcLoc Nothing [] [] (UnQual $ Ident "Enum") [(TyCon (UnQual $ canonicalName))] (genEnums srcLoc (abbrev name) elems)
  , InstDecl srcLoc Nothing [] [] (Qual (ModuleName "Serialize") (Ident "Serialize")) [(TyCon (UnQual $ canonicalName))] []
  ]
  where
    prefix = abbrev $ capitalize name
    canonicalName = Ident $ capitalize name
    decls = map (\(EnumElem (Identifier n) _) -> QualConDecl srcLoc [] [] (ConDecl (Ident $ prefix ++ "_" ++ n) [])) elems
    derivs = [ (UnQual $ Ident "Eq", [])
             , (UnQual $ Ident "Generic", [])
             , (UnQual $ Ident "Ord", [])
             , (UnQual $ Ident "Show", [])
             , (UnQual $ Ident "Typeable", [])
             ]

genEnums :: SrcLoc -> String -> [EnumElem] -> [InstDecl]
genEnums srcLoc prefix elems = (genToEnums srcLoc prefix elems []) ++ (genFromEnums srcLoc prefix elems [])

genToEnums :: SrcLoc -> String -> [EnumElem] -> [InstDecl] -> [InstDecl]
genToEnums _ _ [] soFar = reverse soFar
genToEnums srcLoc prefix ((EnumElem (Identifier n) maybeNum) : es) soFar =
  let decl = InsDecl $ FunBind [Match srcLoc (Ident "toEnum") [PLit Signless $ Int index] Nothing (UnGuardedRhs (Con (UnQual $ Ident name))) Nothing]
  in
    genToEnums srcLoc prefix es (decl : soFar)
  where
    name = prefix ++ "_" ++ n
    index = case maybeNum of
      Just num -> num
      Nothing -> toInteger $ length soFar

genFromEnums :: SrcLoc -> String -> [EnumElem] -> [InstDecl] -> [InstDecl]
genFromEnums _ _ [] soFar = reverse soFar
genFromEnums srcLoc prefix ((EnumElem (Identifier n) maybeNum) : es) soFar =
  let decl = InsDecl $ FunBind [Match srcLoc (Ident "fromEnum") [PApp (UnQual $ Ident name) []] Nothing (UnGuardedRhs (Lit $ Int index)) Nothing]
  in
    genFromEnums srcLoc prefix es (decl : soFar)
  where
    name = prefix ++ "_" ++ n
    index = case maybeNum of
      Just num -> num
      Nothing -> toInteger $ length soFar

genExceptionDef :: SrcLoc -> Definition -> [Decl]
genExceptionDef srcLoc (ExceptionDef ident@(Identifier name) fields) =
  (genStructDef srcLoc (StructDef ident fields)) ++
  [
    InstDecl srcLoc Nothing [] [] (UnQual $ Ident "Exception") [(TyCon (UnQual $ canonicalName))] []
  ]
  where
    canonicalName = Ident $ capitalize name

genDeclarations :: SrcLoc -> Definition -> [Decl]
genDeclarations srcloc def@(TypeDef _ _)      = genTypeDef srcloc def
genDeclarations srcloc def@(ConstDef _ _ _)   = genConstDef srcloc def
genDeclarations srcloc def@(StructDef _ _)    = genStructDef srcloc def
genDeclarations srcloc def@(UnionDef _ _)     = genUnionDef srcloc def
genDeclarations srcloc def@(EnumDef _ _)      = genEnumDef srcloc def
genDeclarations srcloc def@(ExceptionDef _ _) = genExceptionDef srcloc def
genDeclarations _ _ = []

basePragmas :: SrcLoc -> [ModulePragma]
basePragmas srcLoc =
  [
    LanguagePragma srcLoc [Ident "DataKinds"]
  , LanguagePragma srcLoc [Ident "DeriveGeneric"]
  , LanguagePragma srcLoc [Ident "OverloadedStrings"]
  ]

baseImports :: SrcLoc -> [ImportDecl]
baseImports srcloc =
  [
    ImportDecl srcloc (ModuleName "Control.Exception") False False False Nothing Nothing Nothing
  , ImportDecl srcloc (ModuleName "Data.Int") False False False Nothing Nothing Nothing
  , ImportDecl srcloc (ModuleName "Data.Thrift") False False False Nothing Nothing Nothing
  , ImportDecl srcloc (ModuleName "Data.Typeable") False False False Nothing Nothing Nothing
  , ImportDecl srcloc (ModuleName "GHC.Generics") False False False Nothing Nothing Nothing
  , ImportDecl srcloc (ModuleName "Data.Map") True False False Nothing (Just $ ModuleName "M") Nothing
  , ImportDecl srcloc (ModuleName "Data.Serialize") True False False Nothing (Just $ ModuleName "Serialize") Nothing
  , ImportDecl srcloc (ModuleName "Data.Set") True False False Nothing (Just $ ModuleName "S") Nothing
  , ImportDecl srcloc (ModuleName "Data.ByteString.Char8") True False False Nothing (Just $ ModuleName "BS") Nothing
  ]

genModule :: SrcLoc -> Document -> Module
genModule srcloc (Document _ defs) =
  Module srcloc name pragmas Nothing Nothing imports declarations
  where
    srcloc = SrcLoc "foo.hs" 1 1
    name = ModuleName "Foo"
    pragmas = basePragmas srcloc
    imports = baseImports srcloc
    declarations = foldl (\acc d -> acc ++ (genDeclarations srcloc d)) [] defs

createHs :: Document -> FilePath -> IO ()
createHs thriftDoc destHs = do
  let mod = makeHaskell thriftDoc
  writeFile destHs (prettyPrint mod)    

makeHaskell :: Document -> Module
makeHaskell thriftDoc =
  genModule srcloc thriftDoc
  where
    srcloc = SrcLoc "foo.hs" 1 1
