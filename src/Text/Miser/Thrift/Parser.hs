module Text.Miser.Thrift.Parser (parseAll, getIncludePaths) where

import Control.Monad (void, foldM)
import Language.Thrift.Types
import Text.Megaparsec
import Text.Megaparsec.Text
import Text.Miser.Thrift.Lexer

import qualified Data.Map.Strict as M
import qualified Data.Text as T

includeHeader :: Parser Header
includeHeader = do
  reservedWord "include"
  lit <- literal
  return $ IncludeHeader lit

cppIncludeHeader :: Parser Header
cppIncludeHeader = do
  reservedWord "cpp_include"
  lit <- literal
  return $ CppIncludeHeader lit

namespaceScope :: Parser NamespaceScope
namespaceScope = do
  scope <- choice $ map (\s -> symbol s) namespaceScopes
  return $ NamespaceScope scope

namespaceHeader :: Parser Header
namespaceHeader = do
  reservedWord "namespace"
  scope <- namespaceScope
  ident <- identifier
  return $ Namespace scope ident

header :: Parser Header
header = includeHeader <|> cppIncludeHeader <|> namespaceHeader

headers :: Parser [Header]
headers = manyTill (spaceConsumer *> header) (try $ lookAhead $ spaceConsumer *> definition)

identifierFieldType :: Parser FieldType
identifierFieldType = do
  ident <- identifier
  return $ IdentifierFieldType ident

definitionFieldType :: Parser FieldType
definitionFieldType = do
  defType <- definitionType
  return $ DefinitionFieldType defType

fieldType :: Parser FieldType
fieldType = (try definitionFieldType) <|> identifierFieldType

keyValueType :: Parser (FieldType, FieldType)
keyValueType = do
  keyType <- fieldType
  void $ symbol ","
  valueType <- fieldType
  return $ (keyType, valueType)

baseType :: Parser DefinitionType
baseType = do
  base <- choice $ map (\s -> try $ symbol s) baseTypes
  return $ BaseType base

mapType :: Parser DefinitionType
mapType = do
  reservedWord "map"
  (keyType, valueType) <- subType keyValueType
  return $ MapType keyType valueType

listType :: Parser DefinitionType
listType = do
  reservedWord "list"
  valueType <- subType fieldType
  return $ ListType valueType

setType :: Parser DefinitionType
setType = do
  reservedWord "set"
  valueType <- subType fieldType
  return $ SetType valueType

definitionType :: Parser DefinitionType
definitionType = baseType <|> mapType <|> listType <|> setType

typeDef :: Parser Definition
typeDef = do
  reservedWord "typedef"
  defType <- definitionType
  ident   <- identifier
  return $ TypeDef defType ident

fieldId :: Parser FieldId
fieldId = do
  fid <- integer
  void $ symbol ":"
  return $ FieldId fid

requiredField :: Parser FieldQualifier
requiredField = reservedWord "required" >> return RequiredField

optionalField :: Parser FieldQualifier
optionalField = reservedWord "optional" >> return OptionalField

fieldQualifier :: Parser FieldQualifier
fieldQualifier = requiredField <|> optionalField

field :: Parser Field
field = do
  fId <- optional fieldId
  fQualifier <- optional fieldQualifier
  fType <- fieldType
  ident <- identifier
  value <- optional $ (void $ symbol "=") >> constValue
  void $ optional $ listSeparatorConsumer
  return $ Field fId fQualifier fType ident value

fields :: Parser [Field]
fields = many $ spaceConsumer *> field

structDef :: Parser Definition
structDef = do
  reservedWord "struct"
  ident <- identifier
  fs    <- block fields
  return $ StructDef ident fs

unionDef :: Parser Definition
unionDef = do
  reservedWord "union"
  ident <- identifier
  fs    <- block fields
  return $ UnionDef ident fs

enumElem :: Parser EnumElem
enumElem = do
  ident <- identifier
  index <- optional $ (void $ symbol "=") >> integer
  void $ optional $ listSeparatorConsumer
  return $ EnumElem ident index

enumElems :: Parser [EnumElem]
enumElems = many $ spaceConsumer *> enumElem

enumDef :: Parser Definition
enumDef = do
  reservedWord "enum"
  ident <- identifier
  elems <- block enumElems
  return $ EnumDef ident elems

exceptionDef :: Parser Definition
exceptionDef = do
  reservedWord "exception"
  ident <- identifier
  fs    <- block fields
  return $ ExceptionDef ident fs

voidFunctionType :: Parser FunctionType
voidFunctionType = (symbol "void") >> return VoidFunctionType

fieldFunctionType :: Parser FunctionType
fieldFunctionType = do
  ftype <- fieldType
  return $ FunctionType ftype

functionType :: Parser FunctionType
functionType = voidFunctionType <|> fieldFunctionType

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

throws :: Parser Throws
throws = do
  reservedWord "throws"
  fs <- parens fields
  return $ Throws fs

function :: Parser Function
function = do
  oneway <- option False $ (symbol "oneway") >> return True
  ftype <- functionType
  ident <- identifier
  fs    <- parens fields
  throws <- optional $ throws
  void $ optional $ listSeparatorConsumer
  return $ Function oneway ftype ident fs throws

functions :: Parser [Function]
functions = many $ spaceConsumer *> function

extends :: Parser Identifier
extends = reservedWord "extends" >> identifier

serviceDef :: Parser Definition
serviceDef = do
  reservedWord "service"
  ident <- identifier
  ext <- optional extends
  funs <- block functions
  return $ ServiceDef ident ext funs

intConstant :: Parser ConstValue
intConstant = do
  value <- signedInteger
  return $ IntConstant value

doubleConstant :: Parser ConstValue
doubleConstant = do
  value <- signedDouble
  return $ DoubleConstant value

literalConstant :: Parser ConstValue
literalConstant = do
  value <- literal
  return $ LiteralConstant value

identifierConstant :: Parser ConstValue
identifierConstant = do
  value <- identifier
  return $ IdentifierConstant value

listConstant :: Parser ConstValue
listConstant = do
  items <- list constValues
  return $ ListConstant items

constKeyValue :: Parser (ConstValue, ConstValue)
constKeyValue = do
  key <- constValue
  void $ symbol ":"
  value <- constValue
  return $ (key, value)

mapConstant :: Parser ConstValue
mapConstant = do
  kvs <- block $ many (spaceConsumer *> separatedConstKeyValue <* spaceConsumer)
  return $ MapConstant kvs
  where separatedConstKeyValue = do
          kv <- constKeyValue
          void $ optional $ listSeparatorConsumer
          return kv

constValue :: Parser ConstValue
constValue = try doubleConstant <|> intConstant <|> literalConstant <|> identifierConstant <|> listConstant <|> mapConstant

constValues :: Parser [ConstValue]
constValues = many $ spaceConsumer *> separatedConstValue
  where separatedConstValue = do
          value <- constValue
          void $ optional $ listSeparatorConsumer
          return value

constDef :: Parser Definition
constDef = do
  reservedWord "const"
  ftype <- fieldType
  ident <- identifier
  void $ symbol "="
  cval <- constValue
  void $ optional $  listSeparatorConsumer
  return $ ConstDef ftype ident cval

definition :: Parser Definition
definition = try typeDef <|> try structDef <|> unionDef <|> try enumDef <|> exceptionDef <|> serviceDef <|> constDef

definitions :: Parser [Definition]
definitions = manyTill (spaceConsumer *> definition) (try $ lookAhead $ spaceConsumer *> eof)

document :: Parser Document
document = do
  hs <- headers
  ds <- definitions
  return $ Document hs ds

getIncludePaths :: Document -> [FilePath]
getIncludePaths (Document hs _) =
  map (\(IncludeHeader (Literal x)) -> x) includes
  where includes = filter isIncludeHeader hs

isIncludeHeader :: Header -> Bool
isIncludeHeader (IncludeHeader _) = True
isIncludeHeader _                 = False

parseFromFile :: Parsec Dec T.Text Document -> FilePath -> IO (Either (ParseError (Token T.Text) Dec) Document)
parseFromFile p file = runParser p file <$> T.pack <$> readFile file

parseFile :: FilePath -> IO (Either (ParseError (Token T.Text) Dec) Document)
parseFile path = parseFromFile document path

parseAll :: FilePath -> IO (M.Map FilePath (Either (ParseError (Token T.Text) Dec) Document))
parseAll rootPath = parse' [rootPath] M.empty

parse' :: [FilePath] -> M.Map FilePath (Either (ParseError (Token T.Text) Dec) Document) -> IO (M.Map FilePath (Either (ParseError (Token T.Text) Dec) Document))
parse' [] m = return m
parse' (p:ps) m
  | M.member p m = parse' ps m
  | otherwise = do
    doc <- parseFile p
    case (getIncludePaths <$> doc) of
      Left e -> parse' ps (M.insert p doc m)
      Right ds -> parse' (ds ++ ps) (M.insert p doc m)
